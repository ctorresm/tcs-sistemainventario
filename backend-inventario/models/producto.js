var mongoose = require('mongoose');

let Schema = mongoose.Schema;
let autoIncrement = require('mongoose-auto-increment');

autoIncrement.initialize(mongoose.connection);

var productoSchema = Schema({
    nombre: {
        type: String,
        required: [true, 'El nombre es necesario']
    },
    descripcion: {
        type: String,
        required: [true, 'La descripcion es necesaria']
    },
    stock: {
        type: Number,
        required: [true, 'Falta colocar el stock']
    },
    precio: {
        type: Number,
        required: [true, 'Falta colocar el precio']
    }
});
productoSchema.plugin(autoIncrement.plugin, 'codigo');
var Shema = mongoose.model('codigo', productoSchema)
module.exports = mongoose.model('Producto', productoSchema);