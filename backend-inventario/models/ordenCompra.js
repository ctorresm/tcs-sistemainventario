var mongoose = require('mongoose');

let Schema = mongoose.Schema;
let autoIncrement = require('mongoose-auto-increment');

autoIncrement.initialize(mongoose.connection);

mongoose.model('Empleado');
mongoose.model('Proveedor');
mongoose.model('Producto');

var ordenCompraSchema = Schema({
    empleado: { 
        type: Schema.ObjectId, 
        ref: "Empleado",
        required: [true, 'El empleado es necesario'] 
    },
    proveedor: { 
        type: Schema.ObjectId, 
        ref: "Proveedor",
        required: [true, 'El proveedor es necesario']
    },
    descripcion: String,
    fecha: {
        type: String,
        required: [true, 'La fecha es necesaria']
    },
    productos: [{ 
        type: Number, 
        ref: "Producto",
        required: [true, 'El producto es necesario'] 
    }],
    total: {
        type: Number,
        required: [true, 'El total es necesario']
    }
});

ordenCompraSchema.plugin(autoIncrement.plugin, 'numero_compra');
var Shema = mongoose.model('numero_compra', ordenCompraSchema);
module.exports = mongoose.model('OrdenCompra', ordenCompraSchema);