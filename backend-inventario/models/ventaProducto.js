const mongoose = require('mongoose');

let Schema = mongoose.Schema;

mongoose.model('OrdenVenta');
mongoose.model('Producto');

var ventaProductoSchema = Schema({
    venta: {
        type: Number,
        ref: "OrdenVenta",
        required: [true, 'La venta es necesaria']
    },
    producto: {
        type: Number,
        ref: "Producto",
        required: [true, 'El producto es necesario']
    },
    cantidad: {
        type: Number,
        required: [true, 'La cantidad es necesaria']
    },
    estado: {
        type: Boolean,
        default: false,
        required: [true, 'El estado es necesario']
    }
});

module.exports = mongoose.model('VentaProducto', ventaProductoSchema);