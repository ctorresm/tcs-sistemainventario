var mongoose = require('mongoose');

let Schema = mongoose.Schema;

var remisionSchema = Schema({
    numero_remision: {
        type: String, 
        required: [true, 'El numero de remsion es necesario']
    },
    fechaInicio: { 
        type: String, 
        required: [true, 'El fecha de inicio es necesario'] 
    },
    partida: { 
        type: String, 
        required: [true, 'La partidad es necesaria']
    },

    ruc: {
        type: String,
        required: [true, 'El ruc es necesaria']
    },
    
    motivo: {
        type: String,
        required: [true, 'El motivo es necesario']
    },

    rucTrans: {
        type: String,
        required: [true, 'El ruc es necesario']
    },

    denominacion: {
        type: String,
        required: [true, 'La denominación es necesaria']
    },

    marcaPlaca: {
        type: String,
        required: [true, 'La marca y placa son necesarios']
    },

    licencia: {
        type: String,
        required: [true, 'La licencia es necesaria']
    },
    estado: {
        type: Boolean,
        default: false
        //required: [true, 'El estado es necesario']
    },
    cantidad: {
        type: [Number],
        required: [true, 'Las cantidades son necesarias']
    }
});

module.exports = mongoose.model('Remision', remisionSchema);