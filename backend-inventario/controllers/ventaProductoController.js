'use strict'

var VentaProducto = require('../models/ventaProducto');
//var fs = require('fs');
//var path = require('path');

var controller = {

    nuevoVentaProducto: function(req, res) {
        var venta_producto = new VentaProducto();
        var params = req.body;

        venta_producto.venta = params.venta;
        venta_producto.producto = params.producto;
        venta_producto.cantidad = params.cantidad;
        venta_producto.estado = params.estado;

        venta_producto.save((err, venta_productoStored) => {

            if (err) {
                if (!venta_productoStored) return res.status(404).send({ message: 'No se ha podido guardar la venta del producto.' });
                return res.status(500).send({ message: 'Error al guardar el documento.' });
            }

            return res.status(200).send({ venta_producto: venta_productoStored });
        });

        /*
        return res.status(200).send({
            orden_compra: compra_producto,
            message: "Método saveCompraProducto"
        });
        */
    },

    buscarVentaProducto: function(req, res) {
        var venta_productoVenta = req.params.venta;

        if (venta_productoVenta == null) return res.status(404).send({ message: 'La venta del producto no existe' });

        VentaProducto.find({ venta: venta_productoVenta }, (err, venta_producto) => {

            if (!venta_producto) return res.status(404).send({ message: 'La venta del producto no existe.' });

            if (err) {
                return res.status(500).send({ message: 'Error al devolver los datos.' });
            }

            return res.status(200).send({
                venta_producto
            });

        }).populate('venta').populate('producto');
    },

    // buscarCompraProducto: function(req, res){
    //     var compra_productoCompra = req.params.compra;
    //     var compra_productoProducto = req.params.producto;

    //     if(compra_productoCompra == null || compra_productoProducto == null) return res.status(404).send({message: 'La compra del producto no existe'});

    //     CompraProducto.findOne({ compra: compra_productoCompra, producto: compra_productoProducto }, (err, compra_producto) => {

    //         if(!compra_producto) return res.status(404).send({message: 'La compra del producto no existe.'});

    //         if(err) {
    //             return res.status(500).send({message: 'Error al devolver los datos.'});
    //         }

    //         return res.status(200).send({
    //             compra_producto
    //         });

    //     }).populate('compra').populate('producto');
    // },

    listarVentaProductos: function(req, res) {

        VentaProducto.find({}).populate({
            path: 'venta',
            populate: {
                path: 'cliente'
            }
        }).populate('producto').exec((err, ventas_productos) => {

            if (err) {
                if (!ventas_productos) return res.status(404).send({ message: 'No hay ventas de productos que mostrar.' });
                return res.status(500).send({ message: 'Error al devolver los datos.' });
            }

            return res.status(200).send({ ventas_productos });
        });

    },

    editarVentaProducto: function(req, res) {
        var venta_productoId = req.params.id;
        var update = req.body;

        VentaProducto.findByIdAndUpdate(venta_productoId, update, { new: true }, (err, venta_productoUpdated) => {

            if (err) {
                if (!venta_productoUpdated) return res.status(404).send({ message: 'No existe la venta del producto para actualizar' });
                return res.status(500).send({ message: 'Error al actualizar' });
            }

            return res.status(200).send({
                venta_producto: venta_productoUpdated
            });

        });

    },

    borrarVentaProducto: function(req, res) {
        var venta_productoId = req.params.id;

        VentaProducto.findByIdAndRemove(venta_productoId, (err, venta_productoRemoved) => {

            if (err) {
                if (!venta_productoRemoved) return res.status(404).send({ message: "No se puede eliminar esa venta del producto porque no se encuentra." });
                return res.status(500).send({ message: 'No se ha podido borrar la venta del producto' });
            }

            return res.status(200).send({
                venta_producto: venta_productoRemoved
            });
        });
    }
};

module.exports = controller;