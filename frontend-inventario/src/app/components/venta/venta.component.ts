import { Component, Input, OnInit, Inject, ElementRef, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource, MAT_DATE_LOCALE } from '@angular/material';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS } from '@angular/material/core';
import { VentaService } from '../../services/venta.service';
import { ProductoService } from '../../services/producto.service';
import { EmpleadoService } from '../../services/empleados.service';
import { ClienteService } from '../../services/clientes.service';
import { VentaProductoService } from '../../services/venta_producto.service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms'
import { MatPaginatorIntl } from '@angular/material'
import { Observable } from 'rxjs'
import { map, startWith } from 'rxjs/operators'
import { MatPaginatorIntlCro } from '../../utils/matPaginator.util'
import { DatosVenta } from '../../models/datosVenta.model';
import { EmpleadoModel } from '../../models/empleado.model';
import { ProductoModel } from '../../models/producto.model';
import { ClienteModel } from '../../models/cliente.model';
import Swal from 'sweetalert2'
import * as XLSX from 'xlsx';
// import * as jsPDF from 'jspdf';
// import 'jspdf-autotable';

var jsPDF = require('jspdf');
require('jspdf-autotable');
declare var $: any;


@Component({
  selector: 'app-venta',
  templateUrl: './venta.component.html',
  styleUrls: ['./venta.component.css'],
  providers: [{ provide: MAT_DATE_LOCALE, useValue: 'es-ES' },
  { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
  { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }, VentaProductoService, VentaService, ProductoService, ClienteService, EmpleadoService, { provide: MatPaginatorIntl, useClass: MatPaginatorIntlCro },]

})

export class VentaComponent implements OnInit {
  tablaLlena;
  nuevoOrdenVentaForm: FormGroup;
  options: any[] = [];
  options2: any[] = [];
  options3: any[] = [];
  productos: any[] = [];
  cantidad: any[] = [];
  //auxproductos: any []= [];
  //lista: any[] = [];
  ordenventas: any[] = [];
  listarProductosVendidos: any[] = [];
  listprod: any[] = [];
  estado: boolean = false;
  stock: any = "";
  filteredOptions: Observable<ClienteModel[]>;
  filteredOptions2: Observable<EmpleadoModel[]>;
  filteredOptions3: Observable<ProductoModel[]>;
  numeroventa: any;
  ventaProducto: any;
  producto: any;
  //test :any;
  clientes: any;
  empleado: any;
  monto: number;
  fecha = new Date();
  maxDate = new Date();
  @ViewChild('TABLE') table: ElementRef;

  constructor(
    private _ventaService: VentaService,
    private _productoService: ProductoService,
    private _empleadoService: EmpleadoService,
    private _clienteService: ClienteService,
    private _ventaProductoService: VentaProductoService,
    private formBuilder: FormBuilder,
    private _adapter: DateAdapter<any>
  ) { }

  ngOnInit() {
    this.tablaLlena = false;
    this.buildForm();
    this.listarOrdenVentas();
    this.listarVentaProductos();
    this.monto = 0;


    // PARA LLENAR EL ARRAY DE PROVEEDORES EN EL AUTOCOMPLETADO
    this._clienteService.listarClientes().subscribe(result => {
      this.options = result.clientes;
      this.filteredOptions = this.nuevoOrdenVentaForm.controls['cliente'].valueChanges
        .pipe(
          startWith<string | ClienteModel>(''),
          map(value => typeof value === 'string' ? value : ""),
          map(name => name ? this._filter(name) : this.options.slice())
        );

    })

    // PARA LLENAR EL ARRAY DE SOLICITANTES EN EL AUTOCOMPLETADO
    this._empleadoService.listarEmpleados().subscribe(result => {
      this.options2 = result.listaUsuarios;
      this.filteredOptions2 = this.nuevoOrdenVentaForm.controls['solicitante'].valueChanges
        .pipe(
          startWith<string | EmpleadoModel>(''),
          map(value => typeof value === 'string' ? value : ""),
          map(name => name ? this._filter2(name) : this.options2.slice())
        );
    })

    // PARA LLENAR EL ARRAY DE PRODUCTOS EN EL AUTOCOMPLETADO
    this._productoService.listarProductos().subscribe(result => {
      this.options3 = result.productos;
      this.filteredOptions3 = this.nuevoOrdenVentaForm.controls['producto'].valueChanges
        .pipe(
          startWith<string | ProductoModel>(''),
          map(value => typeof value === 'string' ? value : ""),
          map(name => name ? this._filter3(name) : this.options3.slice())
        );
    })
  }

  displayFn(user?: ClienteModel): string | undefined {
    return user ? user.nombres + ' ' + user.apellidos : undefined;
  }

  displayFn2(user?: EmpleadoModel): string | undefined {
    return user ? user.nombres + ' ' + user.apellidos : undefined;
  }

  displayFn3(user?: ProductoModel): string | undefined {
    return user ? user.nombre : undefined;
  }

  exportExcel() {
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(this.table.nativeElement);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

    /* save to file */
    XLSX.writeFile(wb, 'SheetJS.xlsx');
  }

  exportTable() {

    if ((<HTMLInputElement>document.getElementById("fechaInicio")).value == '' || (<HTMLInputElement>document.getElementById("fechaFin")).value == '') {
      Swal.fire({
        type: 'error',
        title: 'Fecha vacía',
        text: 'Complete los campos de Fecha Inicio y fecha Fin'
      })
      return;
    }
    else {
      let newArray: any[] = [];
      let data = Object.values(this.dataSource);
      data = data[8];
      let fecha1 = (<HTMLInputElement>document.getElementById("fechaInicio")).value;
      let fecha2 = (<HTMLInputElement>document.getElementById("fechaFin")).value;

      let f1 = fecha1.split("/")[2] + "-" + fecha1.split("/")[1] + "-" + fecha1.split("/")[0];
      let f2 = fecha2.split("/")[2] + "-" + fecha2.split("/")[1] + "-" + fecha2.split("/")[0];

      if (new Date(f2) < new Date(f1)) {
        Swal.fire({
          type: 'error',
          title: 'Fechas Incorrectas',
          text: 'La fecha final es mayor a la fecha Inicial'
        })
        return;
      }
      else {
        for (let i = 0; i < data.length; i++) {
          let fechaBase = data[i].fecha.split("/")[2] + "-" + data[i].fecha.split("/")[1] + "-" + data[i].fecha.split("/")[0];
          if (new Date(fechaBase) >= new Date(f1) && new Date(fechaBase) <= new Date(f2))
            for (let k = 0; k < data[i].productos.length; k++) {
              newArray.push({
                '# DE ORDEN VENTA': data[i]._id,
                'PRODUCTO VENDIDO': <any>data[i].productos[k].nombre,
                'PRECIO UNITARIO': <any>data[i].productos[k].precio,
                'FECHA DE VENTA': data[i].fecha,
                'VENDEDOR': data[i].empleado.nombres + ' ' + data[i].empleado.apellidos,
                'CLIENTE': data[i].cliente.nombres + ' ' + data[i].cliente.apellidos,
                'EMAIL-CLIENTE': data[i].cliente.email,
                'EMAIL-TELEFONO': data[i].cliente.telefono,
                'TOTAL': data[i].total
              })
            }
        }

        const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(newArray);
        const wb: XLSX.WorkBook = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(wb, ws, 'Book');

        /* save to file */
        XLSX.writeFile(wb, 'REPORTE_VENTAS.xlsx');
      }


    }
  }

  private _filter(name: string): ClienteModel[] {
    const filterValue = name.trim().toLowerCase();
    return this.options.filter(option => option.nombres.toLowerCase().indexOf(filterValue) != -1 ||
      option.apellidos.toLowerCase().indexOf(filterValue) != -1 ||
      option.dni.toLowerCase().indexOf(filterValue) != -1);
  }

  private _filter2(name: string): EmpleadoModel[] {
    const filterValue = name.toLowerCase();
    return this.options2.filter(option2 => option2.nombres.toLowerCase().indexOf(filterValue) != -1 ||
      option2.apellidos.toLowerCase().indexOf(filterValue) != -1 ||
      option2.dni.toLowerCase().indexOf(filterValue) != -1);
  }

  private _filter3(name: string): ProductoModel[] {
    const filterValue = name.trim().toLowerCase();
    return this.options3.filter(option3 => option3._id.toString().toLowerCase().indexOf(filterValue) != -1 ||
      option3.nombre.toLowerCase().indexOf(filterValue) != -1);
  }

  private buildForm() {
    return this.nuevoOrdenVentaForm = this.formBuilder.group({
      cliente: ['', [Validators.required]],
      solicitante: ['', [Validators.required]],
      producto: ['', [Validators.required]],
      descripcion: ['', [Validators]],
      fecha_vent: [new Date(), [Validators.required]],
      total: [0, [Validators.required]],
    });
  }

  displayedColumns: string[] = ['numero_venta', 'cliente', 'solicitante', 'total', 'fecha_vent', 'acciones', 'acciones2'];
  dataSource: MatTableDataSource<DatosVenta>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  get cliente() { return this.nuevoOrdenVentaForm.value.cliente }

  listarOrdenVentas() {
    this._ventaService.listarOrdenVentas().subscribe(
      res => {
        this.ordenventas = res.ordenes_ventas;
        if (this.ordenventas.length == 0) {
          this.numeroventa = 0;
        }
        else {
          this.numeroventa = this.ordenventas[this.ordenventas.length - 1]._id + 1;
        }

        this.dataSource = new MatTableDataSource(res.ordenes_ventas);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      },
      error => {
        console.log(error)
      })
  }

  listarVentaProductos() {
    this._ventaProductoService.listarVentaProductos().subscribe(
      res => {
        this.listarProductosVendidos = res.ventas_productos;
      },
      error => {
        console.log(error)
      })
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  guardarOrdenVenta() {
    if (this.tablaLlena == false) {
      Swal.fire({
        type: 'error',
        title: 'Datos inválidos',
        text: 'Revise nuevamente y llene correctamente los campos.'
      })
      for (let i in this.nuevoOrdenVentaForm.controls)
        this.nuevoOrdenVentaForm.controls[i].markAsTouched();
      return;
    }
    else {
      this.nuevoOrdenVentaForm.controls['fecha_vent'].setValue(this.fecha.getDate() + "/" + (this.fecha.getMonth() + 1) + "/" + this.fecha.getFullYear());
      this.nuevoOrdenVentaForm.controls['total'].setValue(this.monto);
      this.nuevoOrdenVentaForm.controls['producto'].setValue(this.productos);
      this.nuevoOrdenVentaForm.controls['solicitante'].setValue(this.nuevoOrdenVentaForm.value.solicitante._id);
      this.nuevoOrdenVentaForm.controls['cliente'].setValue(this.nuevoOrdenVentaForm.value.cliente._id);


      for (let i = 0; i < this.productos.length; i++) {
        this._ventaProductoService.nuevoVentaProducto({ venta: this.numeroventa, producto: this.productos[i], cantidad: this.cantidad[i], estado: this.estado }).subscribe(
          res => {
          },
          err => { console.log(err); }
        )
      }

      this._ventaService.nuevoOrdenVenta(this.nuevoOrdenVentaForm.value).subscribe(
        res => {
          Swal.fire({
            type: 'success',
            title: 'Orden de venta generada correctamente',
            confirmButtonColor: '#3085d6',
            showConfirmButton: true,
          }).then(
            result => {
              $('#modalNuevaOrdenVenta').modal('hide');
              this.listarOrdenVentas();
              this.nuevoOrdenVentaForm.reset();
              this.productos = [];
              this.cantidad = [];
              this.monto = 0;
              this.stock = null;
              $('.fila').remove();
              this.buildForm();
            }
          )
        },
        err => {
          console.log(err);
        }
      )
    }
  }

  mostrarProducto() {
    if (this.nuevoOrdenVentaForm.invalid) {
      Swal.fire({
        type: 'error',
        title: 'Datos inválidos',
        text: 'Revise nuevamente y llene correctamente los campos.'
      })
      for (let i in this.nuevoOrdenVentaForm.controls)
        this.nuevoOrdenVentaForm.controls[i].markAsTouched();
      return;
    }



    else {
      var formcantidad = (<HTMLFormElement>document.querySelector("#cantidad")).value;
      var cantidad = Number(formcantidad);
      this.producto = this.nuevoOrdenVentaForm.value.producto;

      if (this.producto.stock < cantidad) {
        Swal.fire({
          type: 'error',
          title: 'Stock insuficiente',
          confirmButtonColor: '#3085d6',
          showConfirmButton: true,
        }).then()
      } else {
        this.stock = false;
        Swal.fire({
          type: 'success',
          title: 'Producto agregado',
          confirmButtonColor: '#3085d6',
          showConfirmButton: true,
        }).then((result) => {
          this.tablaLlena = true;
          this.productos.push(this.producto._id);
          this.cantidad.push(cantidad);

          var subtotal = this.producto.precio * cantidad;
          this.monto = this.monto + subtotal;

          var table = document.querySelector("#tabla");
          var tr = document.createElement("tr");
          var td1 = document.createElement("td");
          var td2 = document.createElement("td");
          var td3 = document.createElement("td");
          var td4 = document.createElement("td");

          tr.setAttribute("class", "fila");

          td1.append(this.producto.nombre);
          td2.append(this.producto.descripcion);
          td3.append(String(cantidad));
          td4.append(String(subtotal));

          tr.appendChild(td1);
          tr.appendChild(td2);
          tr.appendChild(td3);
          tr.appendChild(td4);
          table.appendChild(tr);

          $('#cantidad').val("");
          this.nuevoOrdenVentaForm.controls['producto'].setValue("");
        })
      }
    }
  }

  crearReporte(id) {
    var doc = new jsPDF('l', 'mm', 'a4');
    var prodaux;
    var pdfInMM = 210; ``
    var pageCenter = pdfInMM / 2;
    var pageright = 250;
    var pageleft = 30

    for (let i = 0; i < this.listarProductosVendidos.length; i++) {
      if (this.listarProductosVendidos[i].venta._id == id) {
        prodaux = this.listarProductosVendidos[i];
        this.listprod.push(prodaux);
      }
    }

    //TITULO
    doc.setFont('Times', 'bold');
    doc.setFontSize(25);
    doc.text('DISTRIBUIDORA ACERO LINO S.A.C.', pageCenter, 22, 'center');

    doc.setFont('Times', 'bold');
    doc.setFontSize(15);
    doc.text('OFRECE LA VENTA DE MATERIALES DE CONSTRUCCIÓN', pageCenter, 30, 'center');
    doc.text('ELÉCTRICOS, TUBOS, PVC-SANITARIOS EN GENERAL', pageCenter, 35, 'center');

    doc.setFont('Times');
    doc.setFontSize(10);
    doc.text('Email: ferreteria_lino@hotmail.com', pageCenter, 40, 'center');
    doc.text('AV. 1 MZ. A LT. 5 A.V. LA PLANICIE DE LA ERA - ATE - LIMA - LIMA', pageCenter, 47, 'center');
    doc.text('SUCURSAL: AV. ARÉVALO Nº 845 MURUHUAY - SANTA ROSA DE SACCO - YAULI - JUNIN', pageCenter, 52, 'center');
    doc.text('Telf: 7232591 RPM: #941969611 RPC: 980520888', pageCenter, 57, 'center');
    doc.text('__________________________________________________________________________________________', pageleft, 60, 'left')

    //DATOS cliente
    doc.setFont('Times', 'bold');
    doc.setFontSize(12);
    doc.text('Fecha: ' + this.ordenventas[id].fecha + '  ', pageleft, 67, 'left');
    doc.text('D.N.I:  ' + this.ordenventas[id].cliente.dni, 80, 67, 'left');
    doc.text('Señor(es): ' + this.ordenventas[id].cliente.nombres + ' ' + this.ordenventas[id].cliente.apellidos, pageleft, 72, 'left');
    doc.text('Direccion: ' + this.ordenventas[id].cliente.direccion, pageleft, 77, 'left');
    doc.text('__________________________________________________________________________________________________________________', pageleft, 81, 'left')

    //COLUMNA DEL RUC
    doc.autoTable({
      startY: 17,
      margin: { top: 0, right: 23, bottom: 0, left: 193 },
      headStyles: {
        fillColor: [130, 193, 221],
        textColor: [0, 0, 0],
        halign: 'center',
        fontSize: 22,
        font: 'Helvetica',
        fontStyle: 'bold',
        lineColor: [0, 0, 0],
        lineWidth: 0.5
      },
      bodyStyles: {
        fillColor: [242, 245, 247],
        textColor: [0, 0, 0],
        halign: 'center',
        fontSize: 20,
        font: 'Helvetica',
        fontStyle: 'bold',
        lineColor: [0, 0, 0],
        lineWidth: 0.5
      },
      head: [['R.U.C 20601473047 ']],
      body: [['Orden de Venta'], ['Nº ' + this.ordenventas[id]._id]]

    });

    //tabla producto
    doc.autoTable({
      startY: 84,
      headStyles: {
        fillColor: [130, 193, 221],
        textColor: [0, 0, 0],
        halign: 'center',
        fontSize: 13,
        font: 'Helvetica',
        fontStyle: 'bold',
        lineColor: [0, 0, 0],
        lineWidth: 0.2
      },
      bodyStyles: {
        fillColor: [242, 245, 247],
        textColor: [0, 0, 0],
        halign: 'center',
        fontSize: 11,
        font: 'Helvetica',
        fontStyle: '',
        lineColor: [0, 0, 0],
        lineWidth: 0.2
      },
      head: this.headRows(),
      body: this.bodyRows(this.listprod.length)
    });

    doc.text('TOTAL : ' + this.ordenventas[id].total, pageright, 185, 'center');

    // Save the PD  
    doc.text('La venta fue realizada por: ' + this.ordenventas[id].empleado.nombres + ' ' + this.ordenventas[id].empleado.apellidos, pageleft, 198, 'left')
    doc.save('Orden de Venta.pdf');

    this.limpiar();
  }

  headRows() {
    return [{
      Cantidad: 'CANTIDAD',
      Descripcion: 'DESCRIPCION',
      Unitario: 'P. Unitario',
      Importe: 'IMPORTE'
    }];
  }

  bodyRows(rowCount) {
    let aux = [];
    for (let j = 0; j < rowCount; j++) {
      aux.push({
        Cantidad: this.listprod[j].cantidad,
        Descripcion: this.listprod[j].producto.nombre + ' - ' + this.listprod[j].producto.descripcion,
        Unitario: this.listprod[j].producto.precio,
        Importe: this.listprod[j].cantidad * this.listprod[j].producto.precio,
      });
    }
    return aux;
  }

  verStock() {
    if (this.nuevoOrdenVentaForm.value.producto)
      this.stock = this.nuevoOrdenVentaForm.value.producto.stock;
  }

  cerrar() {
    this.nuevoOrdenVentaForm.reset();
    this.productos = [];
    this.cantidad = [];
    this.monto = 0;
    this.stock = "";
    $('.fila').remove();
    this.buildForm();
  }

  limpiar() {
    this.listprod = [];
    this.listarProductosVendidos = [];
    this.ordenventas = [];
    this.listarOrdenVentas();
    this.listarVentaProductos();
  }
}
