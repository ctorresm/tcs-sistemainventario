const mongoose = require('mongoose');

let Schema = mongoose.Schema;

mongoose.model('OrdenCompra');
mongoose.model('Producto');

var compraProductoSchema = Schema({
    compra: {
        type: Number,
        ref: "OrdenCompra",
        required: [true, 'La compra es necesaria']
    },
    producto: {
        type: Number,
        ref: "Producto",
        required: [true, 'El producto es necesario']
    },
    cantidad: {
        type: Number,
        required: [true, 'La cantidad es necesaria']
    },
    estado: {
        type: Boolean,
        default: false,
        required: [true, 'El estado es necesario']
    }
});

module.exports = mongoose.model('CompraProducto', compraProductoSchema);