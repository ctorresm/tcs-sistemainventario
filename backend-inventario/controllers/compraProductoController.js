'use strict'

var CompraProducto = require('../models/compraProducto');
//var fs = require('fs');
//var path = require('path');

var controller = {

    nuevoCompraProducto: function(req, res){
        var compra_producto = new CompraProducto();
        var params = req.body;
        
        compra_producto.compra = params.compra;
        compra_producto.producto = params.producto;
        compra_producto.cantidad = params.cantidad;
        compra_producto.estado = params.estado;

        compra_producto.save((err, compra_productoStored) => {

            if(err) {
                if(!compra_productoStored) return res.status(404).send({message: 'No se ha podido guardar la compra del producto.'});
                return res.status(500).send({message: 'Error al guardar el documento.'});
            }

            return res.status(200).send({compra_producto: compra_productoStored});
        });

        /*
        return res.status(200).send({
            orden_compra: compra_producto,
            message: "Método saveCompraProducto"
        });
        */
    },

    buscarCompraProducto: function(req, res){
        var compra_productoCompra = req.params.compra;

        if(compra_productoCompra == null) return res.status(404).send({message: 'La compra del producto no existe'});

        CompraProducto.find({ compra: compra_productoCompra }, (err, compra_producto) => {
            
            if(!compra_producto) return res.status(404).send({message: 'La compra del producto no existe.'});

            if(err) {
                return res.status(500).send({message: 'Error al devolver los datos.'});
            }

            return res.status(200).send({
                compra_producto
            });

        }).populate('compra').populate('producto');
    },

    // buscarCompraProducto: function(req, res){
    //     var compra_productoCompra = req.params.compra;
    //     var compra_productoProducto = req.params.producto;

    //     if(compra_productoCompra == null || compra_productoProducto == null) return res.status(404).send({message: 'La compra del producto no existe'});

    //     CompraProducto.findOne({ compra: compra_productoCompra, producto: compra_productoProducto }, (err, compra_producto) => {
            
    //         if(!compra_producto) return res.status(404).send({message: 'La compra del producto no existe.'});

    //         if(err) {
    //             return res.status(500).send({message: 'Error al devolver los datos.'});
    //         }

    //         return res.status(200).send({
    //             compra_producto
    //         });

    //     }).populate('compra').populate('producto');
    // },

    listarCompraProductos: function(req, res){

        CompraProducto.find({}).populate('compra').populate('producto').exec((err, compras_productos) => {

            if(err){
                if(!compras_productos) return res.status(404).send({message: 'No hay compras de productos que mostrar.'});
                return res.status(500).send({message: 'Error al devolver los datos.'});
            } 

            return res.status(200).send({compras_productos});
        });

    },

    editarCompraProducto: function(req, res){
        var compra_productoId = req.params.id;
        var update = req.body;

        CompraProducto.findByIdAndUpdate(compra_productoId, update, {new:true}, (err, compra_productoUpdated) => {
            
            if(err){
                if(!compra_productoUpdated) return res.status(404).send({message: 'No existe la compra del producto para actualizar'});
                return res.status(500).send({message: 'Error al actualizar'});
            } 

            return res.status(200).send({
                compra_producto: compra_productoUpdated 
            });

        });

    },

    borrarCompraProducto: function(req, res){
        var compra_productoId = req.params.id;

        CompraProducto.findByIdAndRemove(compra_productoId, (err, compra_productoRemoved) => {

            if(err){
                if(!compra_productoRemoved) return res.status(404).send({message: "No se puede eliminar esa compra del producto porque no se encuentra."});
                return res.status(500).send({message: 'No se ha podido borrar la compra del producto'});
            } 

            return res.status(200).send({
                compra_producto: compra_productoRemoved
            });
        });
    }
};

module.exports = controller;    